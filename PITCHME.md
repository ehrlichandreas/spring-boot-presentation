## Containerization and Microservices

---

#### Long term agenda

- Initialize project
- Git submodules
- Gradle
- Spring Boot
- Docker

+++

- First microservice
- User management microservice
- ReactJS based UI in own microservice
- ELK stack
- Graphana
- Microservice security
- The Kube
- Goint to AWS

---

#### Initialize project

- clone the project
- update subprojects

+++

<span style="color: #e49436">clone the project</span>

```Bash
git clone git@bitbucket.org:ehrlichandreas/spring-boot-presentation.git
```

+++

<span style="color: #e49436">update subprojects</span>

```Bash
gradlew updateSubProjects
```

---

#### Git submodules

---

#### Gradle

---

#### Spring Boot

---

#### Docker

---

#### First microservice (usage tracker)

---

#### User management microservice

---

#### ReactJS based UI in own microservice

---

#### ELK stack

---

#### Graphana

---

#### Microservice security

---

#### The Kube

---

#### Goint to AWS

---